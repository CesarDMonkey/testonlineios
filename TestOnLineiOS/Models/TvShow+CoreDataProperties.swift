import Foundation
import CoreData

extension TvShow {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TvShow> {
        return NSFetchRequest<TvShow>(entityName: "TvShow");
    }
    
    @NSManaged public var tvId: String
    @NSManaged public var medium: String
    @NSManaged public var original: String
    @NSManaged public var name: String
    @NSManaged public var favorite: Bool
    @NSManaged public var summary: String
    @NSManaged public var imdb: String
}
