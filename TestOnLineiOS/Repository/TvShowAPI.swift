//
//  tvShowViewController.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation
import CoreData

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
    case imageCreationError
    case invalidJSONData
}
enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}


private let baseURLString = "http://api.tvmaze.com/shows"

struct TvShowAPI {
    
    static var interestingMoviesURL: URL {
        return flickrURL(parameters: ["extras": ""])
    }
    private static func flickrURL(
        parameters: [String:String]?) -> URL {
        let components = URLComponents(string: baseURLString)!
        return components.url!
    }
    
    static func movies(fromJSON data: Data, into context: NSManagedObjectContext) -> Result<[TvShow],Error> {
        
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            
            guard
                let tvShowsArray = jsonObject as? [AnyHashable] else {
                    return .failure(ErrorResult.invalidJSONData)
            }
            
            var finalDatas = [TvShow]()
            for tvShowJSON in tvShowsArray {
                if let tvShow = tvShow(fromJSON: tvShowJSON as! [String : Any], into: context) {
                    finalDatas.append(tvShow)
                }
            }
            
            if tvShowsArray.isEmpty {
                // We weren't able to parse any of the tvShows.
                // Maybe the JSON format for tvShows has changed.
                return .failure(ErrorResult.invalidJSONData)
            }
            return .success(finalDatas)
        } catch let error {
            return .failure(error)
        }
    }
    private static func tvShow(fromJSON json: [String : Any],
                             into context: NSManagedObjectContext) -> TvShow? {
        
        guard
            let tvId = json["id"] as? Int,
            let name = json["name"] as? String,
            let image = json["image"] as? [String : Any],
            let medium = URL(string: image["medium"] as! String),
            let original = URL(string: image["original"] as! String),
            let summary = json["name"] as? String,
            let externals = json["externals"] as? [String : Any],
            let imdb = externals["imdb"] as? String else {
                
                // Don't have enough information to construct a tvShow
                return nil
        }
        
        let fetchRequest: NSFetchRequest<TvShow> = TvShow.fetchRequest()
        let predicate = NSPredicate(format: "\(#keyPath(TvShow.tvId)) == \(tvId)")
        fetchRequest.predicate = predicate
        
        var fetchedtvShows: [TvShow]?
        context.performAndWait {
            fetchedtvShows = try? fetchRequest.execute()
        }
        if let existingtvShow = fetchedtvShows?.first {
            return existingtvShow
        }
        
        var tvShow: TvShow!
        context.performAndWait {
            tvShow = TvShow(context: context)
            tvShow.tvId = String(tvId)
            tvShow.name = name
            tvShow.medium = medium.absoluteString
            tvShow.original = original.absoluteString
            tvShow.favorite = false
            tvShow.imdb = imdb
            tvShow.summary = summary
        }
        
        return tvShow
    }
}
