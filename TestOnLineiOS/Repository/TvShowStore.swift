//
//  MovieStore.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit
import CoreData

class TvShowStore {
    
    let imageStore = ImageStore()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TvShowCoreData")
        container.loadPersistentStores { (description, error) in
            if let error = error {
                print("Error setting up Core Data (\(error)).")
            }
        }
        return container
    }()
    
    let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
        }()
    
    func processRequest(data: Data?, error: Error?,completion: @escaping (Result<[TvShow],Error>)->Void) {
        guard let jsonData = data else {
            return completion(.failure(error!))
        }
        self.persistentContainer.performBackgroundTask{ (context) in
            let results = TvShowAPI.movies(fromJSON: jsonData, into: context)
            do {
                try context.save()
            } catch {
                print("Error saving to Core Data: \(error).")
                completion(.failure(error))
                return
            }
            switch results {
            case let .success(movies):
                let tvShowIDs = movies.map { return $0.objectID }
                let viewContext = self.persistentContainer.viewContext
                let viewContextTvShows = tvShowIDs.map { return viewContext.object(with: $0) } as! [TvShow]
                completion(.success(viewContextTvShows))
            case .failure(_):
                completion(results)
            }
        }
    }
    
    func processImageRequest(data: Data?, error: Error?) -> Result<UIImage,Error> {
        
        guard
            let imageData = data,
            let image = UIImage(data: imageData) else {
                
                // Couldn't create an image
                if data == nil {
                    return .failure(error!)
                }
                else {
                    return .failure(ErrorResult.imageCreationError)
                }
        }
        
        return .success(image)
    }
    
    func fetchImage(for movie: TvShow?, detail: Bool, completion: @escaping (Result<UIImage,Error>) -> Void) {
        
        let movieKey = movie!.tvId
        if let image = imageStore.image(forKey: movieKey){
            OperationQueue.main.addOperation {
                completion(.success(image))
            }
        }
        let stringURL = detail ? movie!.original : movie!.medium
        let request = URLRequest(url: URL(string: stringURL)!)
        
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            
            let result = self.processImageRequest(data: data, error: error)
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        task.resume()
    }
    
    func fetch(completion: @escaping (Result<[TvShow],Error>) -> Void) {
        
        let url = TvShowAPI.interestingMoviesURL
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            
            self.processRequest(data: data, error: error) {
                (result) in
                
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
        task.resume()
    }
    func fetchLocal(completion: @escaping (Result<[TvShow],Error>) -> Void) {
        let fetchRequest: NSFetchRequest<TvShow> = TvShow.fetchRequest()
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            do {
                let alltvShows = try viewContext.fetch(fetchRequest)
                if(alltvShows.count > 0){
                    completion(.success(alltvShows))
                }else{
                    self.fetch(completion: completion)
                }
            } catch {
                completion(.failure(error))
            }
        }
    }
    func fetchFavorites(completion: @escaping (Result<[TvShow],Error>) -> Void) {
        let fetchRequest: NSFetchRequest<TvShow> = TvShow.fetchRequest()
        let predicate = NSPredicate(format: "favorite == %@", NSNumber(value: true))
        fetchRequest.predicate = predicate
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            do {
                let alltvShows = try viewContext.fetch(fetchRequest)
                completion(.success(alltvShows))
            } catch {
                completion(.failure(error))
            }
        }
    }
    func removeFavorite(_ tv: TvShow){
        let fetchRequest: NSFetchRequest<TvShow> = TvShow.fetchRequest()
        let predicate = NSPredicate(format: "\(#keyPath(TvShow.tvId)) == \(tv.tvId)")
        fetchRequest.predicate = predicate
        self.persistentContainer.performBackgroundTask{ (context) in
            
            var fetchedTvShows: [TvShow]?
            context.performAndWait {
                fetchedTvShows = try? fetchRequest.execute()
            }
            if let existingTvShow = fetchedTvShows?.first {
                existingTvShow.favorite = false
            }
            do {
                try context.save()
            } catch {
                print("Error saving to Core Data: \(error).")
            }
        }
        
    }
    
    func addFavorite(_ tv: TvShow){
        let fetchRequest: NSFetchRequest<TvShow> = TvShow.fetchRequest()
        let predicate = NSPredicate(format: "\(#keyPath(TvShow.tvId)) == \(tv.tvId)")
        fetchRequest.predicate = predicate
        self.persistentContainer.performBackgroundTask{ (context) in
            
            var fetchedTvShows: [TvShow]?
            context.performAndWait {
                fetchedTvShows = try? fetchRequest.execute()
            }
            if let existingTvShow = fetchedTvShows?.first {
                existingTvShow.favorite = true
            }
            do {
                try context.save()
            } catch {
                print("Error saving to Core Data: \(error).")
            }
        }
        
    }
    
}
