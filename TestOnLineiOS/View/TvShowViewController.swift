//
//  FirstViewController.swift
//  TestOnLineiOS
//
//  Created by Administrador on 10/14/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class TvShowViewController: UIViewController, UITableViewDelegate {
    
    
    @IBOutlet weak var tableViewTvShow: UITableView!
    let dataSource = TvShowDataSource()
    lazy var viewModel : TvShowViewModel = {
        let viewModel = TvShowViewModel(dataSource: dataSource)
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TV Shows"
        let nib = UINib(nibName: "BaseTableViewCell", bundle: nil)
        tableViewTvShow.register(nib, forCellReuseIdentifier: "BaseTableViewCell")
        self.tableViewTvShow.delegate = self
        self.tableViewTvShow.dataSource = self.dataSource
        self.dataSource.data.addAndNotify(observer: self) {
            self.tableViewTvShow.reloadData()
            self.refreshControl.endRefreshing()
        }
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error ?
            self!.mostrarAlerta("Oops, algo salió mal!","Hubo un problema al guardar/borrar este show de TV.", completion: {(action) in
                self!.viewModel.fetch()
            })
        }
        tableViewTvShow.addSubview(refreshControl) // not required when using UITableViewController
        if #available(iOS 11.0, *) {
            navigationItem.searchController = resultSearchController
        } else {
            // Fallback on earlier versions
            tableViewTvShow.tableHeaderView = resultSearchController.searchBar
            resultSearchController.searchBar.barTintColor = UIColor.purple
        }
        self.viewModel.fetch()
        self.definesPresentationContext = true
        // Do any additional setup after loading the view.
    }
    override func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        self.viewModel.fetch()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let childVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"DetailViewController") as! DetailViewController
        childVC.tvShow = dataSource.data.value[indexPath.row]
        childVC.viewModel = viewModel
        resultSearchController.searchBar.isHidden = true
        self.navigationController?.pushViewController(childVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.fetch()
        resultSearchController.searchBar.isHidden = false
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,forRowAt indexPath: IndexPath) {
        
        let tvShow = dataSource.data.value[indexPath.row]
        
        // Download the image data, which could take some time
        self.viewModel.fetchImage(for: tvShow,detail: false, completion: { (result) -> Void in
            
            // The index path for the movie might have changed between the
            // time the request started and finished, so find the most
            // interesting index path
            
            guard let movieIndex = self.dataSource.data.value.firstIndex(of: tvShow),
                case let .success(image) = result else {
                    return
            }
            let movieIndexPath = IndexPath(item: movieIndex, section: 0)
            
            // When the request finishes, only update the cell if it's still visible
            if let cell = self.tableViewTvShow.cellForRow(at: movieIndexPath)
                as? BaseTableViewCell {
                cell.update(with: image)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let tvShow = dataSource.isActive ? dataSource.filteredTableData[indexPath.row] : dataSource.data.value[indexPath.row]
        let action: UITableViewRowAction!
        if(tvShow.favorite){
            action = UITableViewRowAction(style: .destructive, title: "Eliminar") { (action, indexPath) in
                // delete item at indexPath
                self.mostrarAlerta("Eliminar","Desea eliminar de favoritos", completion: {(action) in
                    tvShow.favorite = false
                    self.viewModel.removeFavorite(tvShow)
                })
            }
            
        }else{
            action = UITableViewRowAction(style: .default, title: "Favorito") { (action, indexPath) in
                // favorite item at indexPath
                tvShow.favorite = true
                self.viewModel.addFavorite(tvShow)
            }
            action.backgroundColor = UIColor.green
        }
        
        return [action]
        
    }
    override func updateSearchResults(for searchController: UISearchController) {
        let filteredTableData = dataSource.data.value.filter { tvShow in
            return tvShow.name.contains(searchController.searchBar.text!)
        }
        dataSource.filteredTableData = filteredTableData
        dataSource.isActive = resultSearchController.isActive
        tableViewTvShow.reloadData()
    }
}

