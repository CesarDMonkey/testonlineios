//
//  BaseTableViewCell.swift
//  TestOnLineiOS
//
//  Created by Administrador on 10/15/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewTv: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        update(with: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        update(with: nil)
    }
    
    func update(with image: UIImage?) {
        if let imageToDisplay = image {
            spinner.stopAnimating()
            spinner.isHidden = true
            spinner.assignColor(.gray)
            imageViewTv.image = imageToDisplay
            imageViewTv.makeRounded()
        } else {
            spinner.startAnimating()
            spinner.isHidden = false
            imageViewTv.image = nil
        }
    }
    
}
extension UIImageView {
    
    func makeRounded() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 0
        self.clipsToBounds = true
    }
}

extension UIActivityIndicatorView {
    func assignColor(_ color: UIColor) {
        style = .gray
        self.color = color
    }
}

