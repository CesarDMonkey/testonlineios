//
//  SecondViewController.swift
//  TestOnLineiOS
//
//  Created by Administrador on 10/14/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class FavoriteViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    let dataSource = TvShowDataSource()
    lazy var viewModel : TvShowViewModel = {
        let viewModel = TvShowViewModel(dataSource: dataSource)
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Favorites Shows"
        let nib = UINib(nibName: "BaseTableViewCell", bundle: nil)
        tableview.register(nib, forCellReuseIdentifier: "BaseTableViewCell")
        // Do any additional setup after loading the view.
        self.tableview.delegate = self
        self.tableview.dataSource = self.dataSource
        self.dataSource.data.addAndNotify(observer: self) {
            self.tableview.reloadData()
            self.refreshControl.endRefreshing()
        }
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error ?
            self!.mostrarAlerta("Oops, algo salió mal!","Hubo un problema al guardar/borrar este show de TV.", completion: {(action) in
                self!.viewModel.fetchFavorites()
            })
        }
     
        tableview.addSubview(refreshControl) // not required when using UITableViewController
        if #available(iOS 11.0, *) {
            navigationItem.searchController = resultSearchController
        } else {
            // Fallback on earlier versions
            tableview.tableHeaderView = resultSearchController.searchBar
            resultSearchController.searchBar.barTintColor = UIColor.purple
        }
        self.viewModel.fetchFavorites()
        self.definesPresentationContext = true
    }
    override func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        self.viewModel.fetchFavorites()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.fetchFavorites()
        resultSearchController.searchBar.isHidden = false
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,forRowAt indexPath: IndexPath) {
        
        let tvShow = dataSource.isActive ? dataSource.filteredTableData[indexPath.row] : dataSource.data.value[indexPath.row]
        
        // Download the image data, which could take some time
        self.viewModel.fetchImage(for: tvShow, detail: false,completion: { (result) -> Void in
            
            // The index path for the movie might have changed between the
            // time the request started and finished, so find the most
            // interesting index path
            
            guard let movieIndex = self.dataSource.data.value.firstIndex(of: tvShow),
                case let .success(image) = result else {
                    return
            }
            let movieIndexPath = IndexPath(item: movieIndex, section: 0)
            
            // When the request finishes, only update the cell if it's still visible
            if let cell = self.tableview.cellForRow(at: movieIndexPath)
                as? BaseTableViewCell {
                cell.update(with: image)
            }
        })
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let childVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"DetailViewController") as! DetailViewController
        childVC.viewModel = viewModel
        childVC.tvShow = dataSource.isActive ? dataSource.filteredTableData[indexPath.row] : dataSource.data.value[indexPath.row]
        resultSearchController.searchBar.isHidden = true
        self.navigationController?.pushViewController(childVC, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let tvShow = dataSource.isActive ? dataSource.filteredTableData[indexPath.row] : dataSource.data.value[indexPath.row]
        let action: UITableViewRowAction!
        if(tvShow.favorite){
            action = UITableViewRowAction(style: .destructive, title: "Eliminar") { (action, indexPath) in
                // delete item at indexPath
                self.mostrarAlerta("Eliminar","Desea eliminar de favoritos", completion: {(action) in
                    tvShow.favorite = false
                    self.dataSource.data.value.remove(at: indexPath.row)
                    self.viewModel.removeFavorite(tvShow)
                })
            }
            
        }else{
            action = UITableViewRowAction(style: .default, title: "Favorito") { (action, indexPath) in
                // favorite item at indexPath
                tvShow.favorite = true
                self.viewModel.addFavorite(tvShow)
            }
            action.backgroundColor = UIColor.green
        }
        
        return [action]
        
    }
    
    override func updateSearchResults(for searchController: UISearchController) {
        let filteredTableData = dataSource.data.value.filter { tvShow in
            return tvShow.name.contains(searchController.searchBar.text!)
        }
        dataSource.filteredTableData = filteredTableData
        dataSource.isActive = resultSearchController.isActive
        tableview.reloadData()
    }
}

extension UIViewController: UISearchResultsUpdating{
    public func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    var refreshControl:  UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Cargando....")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        return refreshControl
    }
    @objc func refresh(_ sender: AnyObject) {
        
    }
    var resultSearchController: UISearchController {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        return controller
    }
    func mostrarAlerta(_ title: String, _ message: String,completion: @escaping (UIAlertAction) -> Void) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: completion))
        controller.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(controller, animated: true)
    }
}
