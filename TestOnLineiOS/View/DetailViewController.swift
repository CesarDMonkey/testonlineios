//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var txtTitle: UILabel!
    @IBOutlet var txtDuracion: UILabel!
    @IBOutlet var txtFecha: UILabel!
    @IBOutlet var txtCalificacion: UILabel!
    @IBOutlet var txtGenero: UILabel!
    @IBOutlet var txtDescripcion: UILabel!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    var tvShow: TvShow! {
        didSet {
            navigationItem.title = tvShow.name
        }
        willSet{
            
        }
    }
    var viewModel: TvShowViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        spinner.assignColor(.gray)
        self.txtTitle.text = tvShow.name
        self.txtGenero.text = tvShow.imdb
        self.txtDescripcion.text = tvShow.summary.htmlToString
        updateFavorite()
        viewModel.fetchImage(for: tvShow,detail: true, completion: { (result) -> Void in
            switch result {
            case let .success(image):
                self.imageView.image = image
                self.spinner.stopAnimating()
            case let .failure(error):
                print("Error fetching image for movie: \(error)")
            }
        })
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if #available(iOS 11.0, *) {
            navigationItem.searchController?.hidesBottomBarWhenPushed = true
        } else {
            // Fallback on earlier versions
        }
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    @objc func addFavorite(){
        tvShow.favorite = !tvShow.favorite        
        viewModel.addFavorite(tvShow)
        updateFavorite()
    }
    @objc func removeFavorite(){
        tvShow.favorite = !tvShow.favorite
        viewModel.removeFavorite(tvShow)
        updateFavorite()
    }
    func updateFavorite(){
        let favorite = tvShow.favorite ? "baseline_favorite_black_24pt" : "baseline_favorite_border_black_24pt"
        let eventFavorite: Selector = tvShow.favorite ? #selector(removeFavorite) : #selector(addFavorite)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: favorite), style: .plain, target: self, action: eventFavorite)
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
