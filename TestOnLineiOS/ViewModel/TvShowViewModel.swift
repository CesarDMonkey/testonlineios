//
//  TvShowViewModel.swift
//  TestOnLineiOS
//
//  Created by Administrador on 10/14/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation
import UIKit

struct TvShowViewModel {
    
    var dataSource : GenericDataSource<TvShow>?
    var store: TvShowStore?
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    init(dataSource : GenericDataSource<TvShow>?) {
        self.dataSource = dataSource
        self.store = TvShowStore()
    }
    
    func fetch() {
        
        guard let store = self.store else {
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        store.fetchLocal{ (result)-> Void in
            DispatchQueue.main.async {
                switch result {
                case let .success(shows):
                    print("Successfully found \(shows.count) shows.")
                    self.dataSource?.data.value = shows
                case let .failure(error):
                    print("Error fetching recent shows: \(error)")
                    self.dataSource?.data.value.removeAll()
                }
            }
            
        }
    }
    func fetchImage(for tvShow: TvShow, detail: Bool, completion: @escaping (Result<UIImage,Error>) -> Void){
        guard let store = self.store else {
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        store.fetchImage(for: tvShow, detail: detail,completion: completion)
    }
    func addFavorite(_ tv: TvShow){
        store!.addFavorite(tv)
    }
    func removeFavorite(_ tv: TvShow){
        store!.removeFavorite(tv)
    }
    
    func fetchFavorites() {
        
        guard let store = self.store else {
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        store.fetchFavorites{ (result)-> Void in
            DispatchQueue.main.async {
                switch result {
                case let .success(shows):
                    print("Successfully found \(shows.count) shows.")
                    self.dataSource?.data.value = shows
                case let .failure(error):
                    print("Error fetching recent shows: \(error)")
                    self.dataSource?.data.value.removeAll()
                }
            }
            
        }
    }
}
