//
//  TvShowDataSource.swift
//  TestOnLineiOS
//
//  Created by Administrador on 10/14/20.
//  Copyright © 2020 Administrador. All rights reserved.
//
import Foundation
import UIKit

class TvShowDataSource: GenericDataSource<TvShow>, UITableViewDataSource {
    
    var filteredTableData = [TvShow]()
    var isActive:Bool = false
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return isActive ? filteredTableData.count : data.value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "BaseTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier,for: indexPath) as! BaseTableViewCell
        let tv = isActive ? filteredTableData[indexPath.row] : self.data.value[indexPath.row]
        cell.name.text  = tv.name
        return cell
    }
}
