# LEEME #

Este archivo LÉAME normalmente documentaría los pasos necesarios para que su aplicación esté en funcionamiento.

### ¿Para qué es este repositorio? ###

* Como prueba para una vacante de programacion ios para la empresa Grupo Salinas
* Versión 1.0


### Descripción ###

* La siguiente prueba es el desarrollo de una aplicacion que consume servicios para mostrar un listado de Tv Shows

### Cómo se configuro ###

* Instrucciones
* Abrir con Xcode el proyecto.
* Compilar e instalar la aplicacion en un emulador

### Arquitectura empresa ###
* Se utiliza la arquitectura MVVM.
* Se utiliza por que es la aquitectura que mas he empleado o mejor conozco de igual manera conozco el MVP, MVC

### Librerias empleadas ###
* CoreDara para el manejo de datos propio del Xcode no es de terceros

### Qué parte(s) de tu código pueden ser mejoradas si tuvieras más tiempo ###

### Cuáles fueron los mayores problemas que encontraste en el desarrollo de la práctica y cómo los resolviste? ####
* El problema principal fue las vistas son un poco complejas de realizar

### ¿Con quién hablo? ###

* Propietario o administrador del repositorio
* Otra comunidad o contacto de equipo
* Correo electronico: cdzib238@gmail.com